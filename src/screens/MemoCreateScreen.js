import React from 'react';
import {StyleSheet, View, TextInput} from 'react-native';
import CircleButton from '../elements/CircleButton';
import firebase from 'firebase';

class MemoCreateScreen extends React.Component{
    state={
        body: '',
    }
    handlePress(){
        const db = firebase.firestore(); 
        const {currentUser} = firebase.auth();
        db.collection(`users/${currentUser.uid}/memos`).add({
            body: this.state.body,
            createOn: new Date(),
        })
        .then(()=>{
            this.props.navigation.goBack();
            //console.log(this.state.body);
        })
        .catch(()=>{

        });

    }
    render(){
        return(
            <View style={styles.container}>
                <TextInput
                    style={styles.memoEditInput}
                    multiline
                    value={this.state.body}
                    onChangeText={(text)=>{this.setState({body: text});}}
                    textAlignVertical="top"

                />
                <CircleButton onPress={this.handlePress.bind(this)}>
                    {'\uf00c'}
                </CircleButton>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        width: '100%',
    },
    memoEditInput:{
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 32,
        paddingRight: 16,
        paddingLeft: 16,
        paddingBottom: 16,
        fontSize: 16,
    },

});

export default MemoCreateScreen;