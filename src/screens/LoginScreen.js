import React from 'react';
import {StyleSheet, View, Text, TextInput, TouchableHighlight, TouchableOpacity} from 'react-native';
import firebase from 'firebase';
import {StackActions,NavigationActions} from 'react-navigation';

class LoginScreen extends React.Component{
    state={
        email:'tanatopi2015@gmail.com',
        password:'maintain0',
    }
    handleSubmit(){
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            .then((user)=>{
                //console.log('success!',user);
                const resetAction = StackActions.reset({
                    index: 0,
                    actions:[
                        NavigationActions.navigate({routeName:'Home'}),
                    ],
                });
                this.props.navigation.dispatch(resetAction);
            
            })
            .catch(()=>{

            });
    }

    handlepress(){
        this.props.navigation.navigate('Signup');
    }

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.title}>
                    ログイン
                </Text>
                <TextInput
                    style={styles.input}
                    value={this.state.email}
                    onChangeText={(text)=>{this.setState({email: text});}}
                    autoCapitalize='none'
                    autoCorrect={false}
                    placeholder='Email'
                    underlineColorAndroid='transparent'
                />
                <TextInput
                    style={styles.input}
                    value={this.state.password}
                    onChangeText={(text)=>{this.setState({password: text});}}
                    autoCapitalize='none'
                    placeholder='Password'
                    secureTextEntry
                    underlineColorAndroid='transparent'
                />
                <TouchableHighlight style={styles.button} onPress={this.handleSubmit.bind(this)} underlayColor="#c70f66">
                    <Text style={styles.buttonTitle}>ログインする</Text>
                </TouchableHighlight>

                <TouchableOpacity style={styles.signup} onPress={this.handlepress.bind(this)}>
                    <Text style={styles.signupTitle}>メンバー登録</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        width: '100%',
        padding: 24,
        backgroundColor: '#fff',
    },
    input:{
        backgroundColor: '#eee',
        height: 48,
        marginBottom: 16,
        borderWidth: 1,
        borderColor: '#bbb',
        padding: 8,
    },
    title:{
        fontSize: 28,
        alignSelf: 'center',
        marginBottom: 24,
    },
    button:{
        backgroundColor: '#e31676',
        height: 48,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems:'center',
        alignSelf:'center',
        width: '70%',
    },
    buttonTitle:{
        color: '#fff',
        fontSize: 18,
    },
    signup:{
        marginTop: 16,
        alignSelf: 'center',
    },
    signupTitle:{
        fontSize: 16,
    }

});

export default LoginScreen;